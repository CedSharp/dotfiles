#! /usr/bin/env bash
BASH_DIR="$HOME/bash"
source "$BASH_DIR/defaults"
source "$BASH_DIR/exports"
source "$BASH_DIR/aliases"
source "$BASH_DIR/prompt"
source "$BASH_DIR/fix-brew"
