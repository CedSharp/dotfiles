-- Fix Fish Shell bugs
vim.cmd[[
if &shell =~# 'fish$'
  set shell=bash
endif
]]

vim.g.mapleader = ' '

require 'settings'
require 'packer_plugins'
require 'keybindings'

vim.g.vscode_style = 'dark'
vim.cmd [[silent! colorscheme vscode]]

-- Highlight Indent
vim.cmd[[highlight IndentBlanklineChar guifg=#323232 gui=nocombine]]
vim.cmd[[highlight IndentBlanklineContextChar guifg=#808080 gui=nocombine]]

vim.cmd[[
autocmd Filetype javascript,typescript,vue,python,php,json setlocal ts=4 sw=4 sts=0 expandtab
autocmd Filetype html,css,sass,scss,stylus,yaml setlocal ts=2 sw=2 sts=0 expandtab
]]
