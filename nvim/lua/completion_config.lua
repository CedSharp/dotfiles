local ok1, cmp = pcall(require, 'cmp')
local ok2, lspkind = pcall(require, 'lspkind')

if not ok1 or not ok2 then
  return nil
end

vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

cmp.setup({
  formatting = {
    format = lspkind.cmp_format({
      with_text = false,
      maxwidth = 50,
      before = function(entry, vim_item)
        return vim_item
      end,
    }),
  },
  snippet = {
    expand = function(args)
      require'luasnip'.lsp_expand(args.body)
    end,
  },
  mapping = {
    ['<C-Spance>'] = cmp.mapping(cmp.mapping.complete(), {'i', 'c'}),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  },{ name = 'buffer' }),
})

cmp.setup.cmdline('/', {sources = {{ name = 'buffer' }}})
cmp.setup.cmdline(':', {
  sources = cmp.config.sources(
    {{ name = 'path' }},
    {{ name = 'cmdline'}}
  )
})

