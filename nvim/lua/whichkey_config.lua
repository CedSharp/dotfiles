local wk = require 'which-key'

wk.register({
  g = {
    name = 'LSP',
    D = {'<cmd>lua vim.lsp.buf.declaration()<CR>', 'Declaration'},
    d = {'<cmd>lua vim.lsp.buf.definition()<CR>', 'Definition'},
    i = {'<cmd>lua vim.lsp.buf.implementation()<CR>', 'Implementation'},
    r = {'<cmd>lua vim.lsp.buf.references()<CR>', 'References'},
    R = {'<cmd>lua vim.lsp.buf.rename()<CR>', 'Rename'},
    a = {'<cmd>lua vim.lsp.buf.code_action()<CR>', 'Code Action'},
    l = {'<cmd>lua vim.lsp.buf.formatting()<CR>', 'Format'},
  }
})

wk.setup()
