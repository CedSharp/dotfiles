local M = {}

M.toggle = function()
  if (require'bufferline.state'.offset == 0) then
    M.open()
  else
    M.close()
  end
end

M.open = function()
  require'bufferline.state'.set_offset(30, 'Explorer')
  require'nvim-tree'.find_file(true)
end

M.close = function()
  require'bufferline.state'.set_offset(0)
  require'nvim-tree'.close()
end

M.setup = function()
  local map = require'utils'.map
  -- Toggle Explorer
  map('n', '<leader>e', ":lua require'barbar_config'.toggle()<CR>")
  -- Switch tabs
  map('n', '<TAB>', ':BufferNext<CR>')
  map('n', '<S-TAB>', ':BufferPrevious<CR>')
  map('n', '<leader>q', ':BufferClose<CR>')
end

return M
