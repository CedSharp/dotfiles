vim.g.indent_blankline_use_treesitter = true

local opts = {
  termguicolors = true,
  background = 'dark',
  cursorline = true,
  splitright = true,
  splitbelow = true,
  tabstop = 4,
  shfitwidth = 4,
  expandtab = true,
}

for key, val in ipairs(opts) do
  vim.o[key] = val
end
