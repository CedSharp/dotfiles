local ok, indent = pcall(require, 'indent_blankline')
if ok then
  indent.setup {
    show_current_context = true,
    show_current_context_start = false,
  }
end
