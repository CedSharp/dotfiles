local map = require'utils'.map
local telescope = pcall(require, 'telescope')

if ok then
  telescope.setup{}

  map('n', '<leader>ff', ':Telescope find_files<CR>')
  map('n', '<leader>fg', ':Telescope live_grep<CR>')
  map('n', '<leader>fb', ':Telescope buffers<CR>')
  map('n', '<leader>fh', ':Telescope help_tags<CR>')
end
