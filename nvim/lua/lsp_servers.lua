local ok, lsp = pcall(require, 'lspconfig')

if not ok then
  return nil
end

local capabilities = require'cmp_nvim_lsp'.update_capabilities(
  vim.lsp.protocol.make_client_capabilities()
)

function on_attach(client, bufnr)
  require'lsp_signature'.on_attach()

  local opts = { noremap = true, silent = true }
  local function bmap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function bopt(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  bopt('omnifunc', 'v:lua.vim.lsp.omnifunc')
  bmap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  bmap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  bmap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  bmap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  bmap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  bmap('n', 'gR', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  bmap('n', 'ga', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  bmap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
  bmap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
  bmap('n', 'gl', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

end

-- Volar (js, ts, vue, json)
lsp.volar.setup {
  capabilities = capabilities,
  on_attach = on_attach,
  filetypes = {'typescript', 'javascript', 'typescriptreact', 'javascriptreact', 'vue', 'json'},
}

-- Emmet
if not lsp.emmet_ls then
  lsp.emmet_ls = {
    default_config = {
      cmd = {'emmet-ls', '--stdio'},
      filetypes = {'html', 'css', 'scss'},
      root_dir = function(fname)
        return vim.loop.cwd()
      end,
      settings = {},
    },
  }
end
lsp.emmet_ls.setup {
  capabilities = capabilities,
  on_attach = on_attach,
}

-- Lua
local lua_runtime_path = vim.split(package.path, ';')
table.insert(lua_runtime_path, 'lua/?.lua')
table.insert(lua_runtime_path, 'lua/?/init.lua')
lsp.sumneko_lua.setup {
  capabilities = capabilities,
  on_attach = on_attach,
  settings = {
    Lua = {
      runtime = {
        version = 'LuaJIT',
        path = lua_runtime_path,
      },
      diagnostics = {
        globals = {'vim'},
      },
      workspace = {
        library = vim.api.nvim_get_runtime_file('', true),
      },
      telemetry = {
        enable = false,
      },
    },
  },
}
