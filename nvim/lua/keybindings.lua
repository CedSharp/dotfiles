local map = require'utils'.map

map('n', '<leader>h', '<C-w><C-h>')
map('n', '<leader>j', '<C-w><C-j>')
map('n', '<leader>k', '<C-w><C-k>')
map('n', '<leader>l', '<C-w><C-l>')
