local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
local packer_bootstrap = false

if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({
    'git', 'clone', '--depth', '1',
    'https://github.com/wbthomason/packer.nvim',
    install_path
  })

  vim.cmd [[packadd! packer.nvim]]
end

return require'packer'.startup(function(use)
  -- Allow packer to manage itself
  use 'wbthomason/packer.nvim'

  -- Dependencies
  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
  use {'kyazdani42/nvim-web-devicons'}

  -- Sensible settings
  use {'jeffkreeftmeijer/neovim-sensible'}

  -- Color scheme
  use 'Mofiqul/vscode.nvim'

  -- Layout
  use {'kyazdani42/nvim-tree.lua', after = 'nvim-web-devicons',
       config = function() require 'nvimtree_config' end,
  }
  use {'romgrk/barbar.nvim', after = 'nvim-web-devicons',
       setup = function() require'barbar_config'.setup() end,
  }
  use {'nvim-lualine/lualine.nvim', after = 'nvim-web-devicons',
       config = function() require'lualine_config' end
  }

  -- Additional languages
  use 'ChiliConSql/neovim-stylus'
  use 'khaveesh/vim-fish-syntax'

  -- Git
  use {'TimUntersberger/neogit', requires = {'nvim-lua/plenary.nvim'}}

  -- Nvim LSP
  use {'neovim/nvim-lspconfig'}
  use {'ray-x/lsp_signature.nvim', after = 'nvim-lspconfig'}
  use {'onsails/lspkind-nvim'}
  use {'hrsh7th/cmp-nvim-lsp', after = 'nvim-lspconfig'}
  use {'hrsh7th/cmp-buffer', after = 'nvim-lspconfig'}
  use {'hrsh7th/cmp-path', after = 'nvim-lspconfig'}
  use {'hrsh7th/cmp-cmdline', after = 'nvim-lspconfig'}
  use {'hrsh7th/nvim-cmp', after = {
    'lsp_signature.nvim',
    'lspkind-nvim',
    'cmp-nvim-lsp',
    'cmp-buffer',
    'cmp-path',
    'cmp-cmdline'},
    config = function()
      require'completion_config'
      require'lsp_servers'
    end,
  }
  use {'folke/trouble.nvim', after = {'nvim-lspconfig', 'nvim-web-devicons'},
       config = function()
         local trouble = pcall(require, 'trouble')
         if ok then
           trouble.setup {}
         end
       end,
  }

  -- Formatting
  use 'editorconfig/editorconfig-vim'

  -- Snippets
  use {'saadparwaiz1/cmp_luasnip', requires = {'L3MON4D3/LuaSnip'}}

  -- Telescope stuff
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make'}
  use {'nvim-telescope/telescope.nvim', requires = {{'nvim-lua/plenary.nvim'}},
       after = {'nvim-treesitter', 'nvim-web-devicons'},
       config = function() require'config_telescope' end,
  }

  -- Indentation Guide
  use {'lukas-reineke/indent-blankline.nvim',
       config = function() require'indent_config' end,
  }

  -- Rainbow pairs (bracket, tags, etc)
  use {'p00f/nvim-ts-rainbow', after = 'nvim-treesitter',
       setup = function() require'pairs_config' end,
  }

  -- Helper cuz I'm dumb
  use {'folke/which-key.nvim',
       config = function() require'whichkey_config' end,
  }
end)
