local ok, ts_config = pcall(require, 'nvim-treesitter.configs')
if ok then
  ts_config.setup {
    ensure_installed = {
      'typescript',
      'javascript',
      'html',
      'json',
      'yaml',
      'scss',
      'css',
      'vue',
      'vim',
      'lua',
      'rust',
      'python',
      'php',
      'make',
      'jsdoc',
      'java',
      'fish',
      'bash',
      'dockerfile',
      'c_sharp',
    },
    rainbow = {
      enable = true,
      extended_mode = true,
      max_file_lines = nil,
    }
  }
end
