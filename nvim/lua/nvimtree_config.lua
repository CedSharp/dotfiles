local map = require 'utils'.map
local ok, tree = pcall(require, 'nvim-tree')

if ok then
  tree.setup {
    auto_close = true,
    open_on_setup = true,
    diagnostics = { enable = true },
  }
end
