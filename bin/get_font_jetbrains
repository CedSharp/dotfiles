#! /usr/bin/env bash

# Make sure this is linux
if [ ! "$(uname)" == "Linux" ]; then
  echo "This script only supports installing fonts for Linux."
  echo "You can download and install the font yourself via this url:"
  echo https://github.com/ryanoasis/netd-fonts/releases/download/v2.1.0/JetBrainsMono.zip
  exit 0
fi

# Make sure some libraries are available
if [ "$(which wget)" == "" ]; then
  echo "Please install 'wget' in order to install fonts"
  exit 1
fi

if [ "$(which unzip)" == "" ]; then
  echo "Please install 'unzip' in order to install fonts"
  exit 1
fi


echo "Downloading JetBrainsMono.zip from nerdfonts.com ..."
rm -rf /tmp/JetBrainsMono.zip
wget -P /tmp/ https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip &> /dev/null

if [ ! -f "/tmp/JetBrainsMono.zip" ]; then
  echo "An error occurred while downloading the font, make sure you are connected to the internet and try again"
  exit 1
fi


echo "Extracting JetBrainsMono.zip ..."
mkdir -p /tmp/jetbrains-fonts
rm -rf /tmp/jetbrains-fonts/*
unzip /tmp/JetBrainsMono.zip -d /tmp/jetbrains-fonts &> /dev/null

COUNT="$(ls /tmp/jetbrains-fonts 2> /dev/null | wc -l)"
if [ "$COUNT" == "0" ]; then
  echo "Error extracting files and I don't know why!"
  exit 1
fi

echo "Adding fonts to '$HOME/.local/share/fonts' ..."
mkdir -p $HOME/.local/share/fonts
mv /tmp/jetbrains-fonts/* $HOME/.local/share/fonts/

echo "Updating fonts cache ..."
fc-cache -f -v &> /dev/null

echo "Making sure JetBrainsMono exists ..."
INSTALLED_COUNT="$(fc-list | grep JetBrainsMono | wc -l)"
if [ "$INSTALLED_COUNT" != "$COUNT" ]; then
  echo "The number of fonts installed does not match the number of files that were installed."
  echo "This does not mean it failed, just that this tool cannot validate if the font is properly installed"
else
  echo ""
  echo ""
  echo "--[ The font is properly installed! ]--"
  echo ""
fi

echo "Cleaning up ..."
rm -rf /tmp/JetBrainsMono.zip
rm -rf /tmp/jetbrains-fonts

echo "Done!"
exit 0
