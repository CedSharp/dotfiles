# DotFiles

My linux/mac dotfile configurations

## How to use

 1. Clone this repository to any directory that suits you;

 2. Navigate to the `bin` folder and run `install_dotfiles` command;

 3. This will backup your `.bashrc` file, create a `.bin` and `bash`
 directory in your `$HOME`;

 4. Then, you can run `get_lsd` to get colors+icons when using `ls`;

 5. You can also run `get_micro` if you want nano editor with normal keybinds,
 line numbers and syntax highlight;

 6. If you have a PragmataPro font license, you can automatically install it
 as follow:

    1. Download the different family zip files from your account at https://fsd.it;

    2. Create a folder at the root of this repository called `fonts` and
    place the zips of the fonts in it;

    3. From the root of the repository, execute `bin/install_pragmata`;

 7. If you prefer a VIM-like editor, you can execute `bin/install_neovim`.
 Note this will install my configurations and plugins. If you already have
 a neovim configuration, it will be backed up to `~/.config/nvim-backup`.
